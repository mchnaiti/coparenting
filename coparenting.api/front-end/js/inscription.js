﻿$("#tel")
  .bind('focusout', function (e) {
      var tel = $(this).val();
      if (tel.length < 11) {
          if (!validateTel(tel)) {
              e.preventDefault();
              $(this).css("background-color", "red");
              $(this).focus();

          } else {
              $(this).css("background-color", "white");
          }
      } else {
          $(this).css("background-color", "red");
          e.preventDefault();
          $(this).focus();
      }
    
  });
function validateTel(tel) {
    var re = /(438|514)([0-9]{7,7})/i;
    return re.test(tel);
}
$('#btnRegister').click(function () {

    // appel ajax pour inscription
    var sURL = "http://localhost:13340/api/Account/Register";
    var email = $('#courriel').val();
    var password = $('#mpasse').val();
    var confirmPassword = $('#confirmationMpasse').val();
    var lastName = $('#nom').val();
    var firstName = $('#prenom').val();
    var phone = $('#tel').val();
    var coparent = $('#coparent').val();

    var oInscription = {
        Email: email,
        Password: password,
        ConfirmPassword: confirmPassword,
        LastName: lastName,
        FirstName: firstName,
        Phone: phone,
        Coparent: coparent
    };
    var showError = function (err) {
        var msgError = "Erreur: veuillez entrer les bonnes valeurs \n(Details:" + JSON.stringify(err.responseJSON.ModelState) + ")";
        $("#messageError").text(msgError);
        return false;
    }
    $.ajax({
        url: sURL,
        async: true,
        dataType: "json",
        cache: false,
        data: oInscription,
        type: "POST"
    }).done(function (data) {
        $("#messageSuccess").text("enregistrer avec succés");
        location.href = "index.html";
    }).fail(showError);

});
$("#login").submit(function (event) {
    event.preventDefault();
    var email = $('#courriel').val();
    var password = $('#mpasse').val();
    var loginData = {
        grant_type: 'password',
        username: email,
        password: password
    };

    var showError = function (err) {
        alert(err.responseJSON.error_description);
        return false;
    }

    $.ajax({
        type: 'POST',
        url: '/Token',
        data: loginData
    }).done(function (data) {
        console.log(data);
        // Cache the access token in session storage.
        var tokenKey = "userToken";
        sessionStorage.setItem(tokenKey, data.access_token);
        sessionStorage.setItem('username', data.userName);
        location.href = "calendrier.html";
    }).fail(showError);
    //.error(showError);

    return true;
});
