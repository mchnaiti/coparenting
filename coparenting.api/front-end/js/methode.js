var _events = {
    "2012-03-03": {
        "number": 1,
        "badgeClass": "badge-warning",
        "dayEvents": [
            {
                "name": "Important meeting",
                "hour": "17:30"
            },
            {
                "name": "Morning meeting at coffee house",
                "hour": "08:15"
            }
        ]

    }

};
/////////////////////////////////////////////////////////////////////////////////////////

$(document).ready(function () {
    var userName = sessionStorage['username'];
    $('#userName').html(userName);
    function addLeadingZero(num) {
        if (num < 10) {
            return "0" + num;
        } else {
            return "" + num;
        }
    }



    $(".responsive-calendar").responsiveCalendar({
        onDayClick: function (events) {
            var thisDayEvent, key;

            key = $(this).data('year') + '-' + addLeadingZero($(this).data('month')) + '-' + addLeadingZero($(this).data('day'));
            thisDayEvent = events[key];

            if (thisDayEvent.dayEvents[0].type === 'horaireScolaire') {

                $("#vueEvenScolaire").modal('show');
                $("#idEvenement").attr("value", thisDayEvent.dayEvents[0].idEvent);
                $("#titreScolaire").attr("value", thisDayEvent.dayEvents[0].name);
                $("#FreqScolaire").attr("value", thisDayEvent.dayEvents[0].frequence);
                $("#deScolaire").attr("value", thisDayEvent.dayEvents[0].date);
                $("#aScolaire").attr("value", thisDayEvent.dayEvents[0].fin);
                $("#noteScolaire").attr("value", thisDayEvent.dayEvents[0].commentaires);

                /*$("#titreScolaire").val(thisDayEvent.dayEvents[0].name);
                 $("#FreqScolaire").val(thisDayEvent.dayEvents[0].frequence);
                 $("#deScolaire").val(thisDayEvent.dayEvents[0].date);
                 $("#aScolaire").val(thisDayEvent.dayEvents[0].fin);
                 $("#noteScolaire").val(thisDayEvent.dayEvents[0].commentaires);*/

            } else if (thisDayEvent.dayEvents[0].type === 'horaireDeGarde') {
                $("#vueEvenGarde").modal('show');
                $("#idEvenement").attr("value", thisDayEvent.dayEvents[0].idEvent);
                $("#titreGarde").attr("value", thisDayEvent.dayEvents[0].name);
                $("#nomGard").attr("value", thisDayEvent.dayEvents[0].nomGardien);
                $("#dateGarde").attr("value", thisDayEvent.dayEvents[0].date);
                $("#heureGarde").attr("value", thisDayEvent.dayEvents[0].heure);
                $("#noteGarde").attr("value", thisDayEvent.dayEvents[0].commentaires);
                $("#frequencee").attr("value", thisDayEvent.dayEvents[0].frequence);
                /* $("#titreGarde").val(thisDayEvent.dayEvents[0].name);
                 $("#nomGard").val(thisDayEvent.dayEvents[0].nomGardien);
                 $("#dateGarde").val(thisDayEvent.dayEvents[0].date);
                 $("#heureGarde").val(thisDayEvent.dayEvents[0].heure);
                 $("#noteGarde").val(thisDayEvent.dayEvents[0].commentaires);*/

            } else if (thisDayEvent.dayEvents[0].type === 'rdv') {
                $("#vuerdv").modal('show');
                $("#idEvenement").attr("value", thisDayEvent.dayEvents[0].idEvent);
                $("#titrerdv").attr("value", thisDayEvent.dayEvents[0].Titre);
                $("#daterdv").attr("value", thisDayEvent.dayEvents[0].Date);
                $("#ardv").attr("value", thisDayEvent.dayEvents[0].heure);
                $("#endroit").attr("value", thisDayEvent.dayEvents[0].Adresse);
                $("#nomRdv").attr("value", thisDayEvent.dayEvents[0].NomPersonneRencontree);
                $("#telrdv").attr("value", thisDayEvent.dayEvents[0].TelPersonneRencontree);
                $("#noterdv").attr("value", thisDayEvent.dayEvents[0].commentaires);
                /*$("#titrerdv").val(thisDayEvent.dayEvents[0].name);
                 $("#daterdv").val(thisDayEvent.dayEvents[0].date);
                 $("#ardv").val(thisDayEvent.dayEvents[0].heure);
                 $("#endroit").val(thisDayEvent.dayEvents[0].adresse);
                 $("#nomRdv").val(thisDayEvent.dayEvents[0].nomRDV);
                 $("#telrdv").val(thisDayEvent.dayEvents[0].tel);
                 $("#noterdv").val(thisDayEvent.dayEvents[0].commentaires);*/

            } else if (thisDayEvent.dayEvents[0].type === 'horaireVacance') {

                $("#vuevacances").modal('show');
                $("#titreVac").attr("value", thisDayEvent.dayEvents[0].name);
                $("#deVac").attr("value", thisDayEvent.dayEvents[0].debut);
                $("#aVac").attr("value", thisDayEvent.dayEvents[0].fin);
                $("#avecVac").attr("value", thisDayEvent.dayEvents[0].personneAccompagnon);
                $("#noteVac").attr("value", thisDayEvent.dayEvents[0].commentaires);

                /* $("#titreVac").val(thisDayEvent.dayEvents[0].name);
                 $("#deVac").val(thisDayEvent.dayEvents[0].debut);
                 $("#aVac").val(thisDayEvent.dayEvents[0].fin);
                 $("#avecVac").val(thisDayEvent.dayEvents[0].avec);
                 $("#noteVac").val(thisDayEvent.dayEvents[0].commentaires);*/
            }

        }
    });
}
);
////////////////////////
$(document).ready(function () {
    $('#choixtype').click(function () {
        var choix = ($("#type option:selected").val());
        if (choix === 'horaireScolaire') {
            $('#titreEvenement').siblings().not("#joursCours,#debutSession,#finSession,#commentaires,#ajouterEvenement,#choix").hide();
            $("#titreEvenement").slideDown("slow");
            $("#joursCours").slideDown("slow");

            $("#debutSession").slideDown("slow");
            $("#finSession").slideDown("slow");


            $("#commentaires").slideDown("slow");
            $("#ajouterEvenement").slideDown("slow");
        } else if (choix === 'horaireDeGarde') {
            $('#titreEvenement').siblings().not("#hidegardePar,#dateHeureEvenement,#commentaires,#ajouterEvenement,#choix").hide();

            $("#titreEvenement").slideDown("slow");
            $("#hidegardePar").slideDown("slow");
            $("#dateHeureEvenement").slideDown("slow");
            $("#frequenceDeGarde").slideDown("slow");
            $("#commentaires").slideDown("slow");
            $("#ajouterEvenement").slideDown("slow");






        } else if (choix === 'rdv') {

            $('#telephone').siblings().not("#titreEvenement,#dateHeureEvenement,#lieuEvenement,#nomPersonne,#telephone,#commentaires,#ajouterEvenement,#choix").hide();
            $("#titreEvenement").slideDown("slow");
            $("#dateHeureEvenement").slideDown("slow");
            $("#lieuEvenement").slideDown("slow");
            $("#nomPersonne").slideDown("slow");
            $("#telephone").slideDown("slow");
            $("#commentaires").slideDown("slow");
            $("#ajouterEvenement").slideDown("slow");

        } else if (choix === 'horaireVacance') {
            $('#titreEvenement').siblings().not("#debutVacances,#finVacances,#vacancesGarde,#commentaires,#ajouterEvenement,#choix").hide();

            $("#titreEvenement").slideDown("slow");
            $("#debutVacances").slideDown("slow");
            $("#finVacances").slideDown("slow");
            $("#vacancesGarde").slideDown("slow");
            $("#commentaires").slideDown("slow");
            $("#ajouterEvenement").slideDown("slow");

        }
    }
    )
    ////////////////////////////////////////////

    $(function () {
        $("#datepicker,#datepicker1,#datepicker2,#datepicker3,#datepicker4,#datepicker5,#datepicker6").datepicker({
            autoclose: true
        });
    });

    ////////////////////////////////////////////

    $('#timepicker1').timepicker({
        showMeridian: false,
    });
    //////////////////////////////////////

    $('#submit').click(function () {
        $('#choix').siblings().hide();
        choix = ($("#type option:selected").val());

        $('#ajoutEvenement').modal('hide');
        var element = {};
        element.number = 1;
        element.badgeClass = "badge-warning";
        element.type = choix;
        element.dayEvents = [];
        var nomEvenement = $('#titre').val();
        var commentaire = $('#commentaire').val();

        if (choix === 'horaireScolaire') {
            var debut = $('#datepicker').val();
            var fin = $('#datepicker2').val();
            var frequence = ($("#joursdeCours option:selected").val());

            if (commentaire === '')
                commentaire = 'Aucun commentaire';

            //  $("#tablehistorique tr:first").after("<tr><td>2016-04-20</td><td>Horaire scolaire(VIENT D'ETRE AJOUTE)</td><td>Titre: " + nomEvenement + "<br>\
            //  Frequence: 	" + frequence + "<br>Debut de l'année scolaire: " + debut + "<br>Fin de l'année scolaire: " + fin + "<br>\
            //  Commentaires: " + commentaire + "<br></td><td>Parent1</td><td>Ajout</td></tr>");

            // appel ajax pour ajouter évènement dans Horaire scolaire
            var sURL = "http://localhost:13340/api/HoraireScolaire";
            var oEvenement = {
                Titre: nomEvenement,
                JoursDeCours: frequence,
                DebutSession: debut,
                FinSession: fin,
                Commentaire: commentaire
            };

            $.ajax({
                url: sURL,
                async: true,
                dataType: "json",
                cache: false,
                data: oEvenement,
                type: "POST",
                success: function (oData) {
                    element.dayEvents.push({ "idEvent": oData.IdEvenement, "type": choix, "date": oData.DebutSession, "fin": oData.FinSession, "name": oData.Titre, "commentaires": oData.Commentaire });
                    _events[debut] = element;
                    _events[fin] = element;
                    $(".responsive-calendar").responsiveCalendar('edit', _events);
                    ajouterNotification('horaire', oData);
                },
                error: function (err) {
                    alert(JSON.stringify(err));
                }
            });

        } else if (choix === 'horaireDeGarde') {
            var nomGardien = $('#gardePar').val();
            var freqGarde = $('#frequenceGarde').val();

            var dateGarde = $('#datepicker6').val();
            var heureGarde = $('#timepicker1').val();
            if (commentaire === '')
                commentaire = 'Aucun commentaire';
            //element.dayEvents.push({                
            //    "type": choix, "name": nomEvenement, "nomGardien": nomGardien, 'date': dateGarde,
            //    'heure': heureGarde, 'frequence': freqGarde, "commentaires": commentaire
            //});
            //_events[dateGarde] = element;
            //$(".responsive-calendar").responsiveCalendar('edit', _events);

            //$("#tablehistorique tr:first").after("<tr><td>2016-04-20</td><td>Horaire de garde(VIENT D'ETRE AJOUTE)</td><td>Titre: " + nomEvenement + "<br>\
            //Gardé par: 	" + nomGardien + "<br>Le: " + dateGarde + "<br>A: " + heureGarde + "<br>Fréquence de garde: " + freqGarde + "<br>Commentaires: " + commentaire + "<br></td><td>Parent1</td><td>Ajout</td></tr>");

            // appel ajax pour ajouter évènement dans HoraireGarde
            var sURL = "http://localhost:13340/api/HoraireGarde";
            var oEvenement = {
                Titre: nomEvenement,
                Commentaire: commentaire,
                GardePar: nomGardien,
                Frequence: freqGarde,
                Date: dateGarde
            };
            $.ajax({
                url: sURL,
                async: true,
                dataType: "json",
                cache: false,
                data: oEvenement,
                type: "POST",
                success: function (oData) {
                    element.dayEvents.push({
                        "idEvent": oData.IdEvenement,
                        "type": choix, "name": oData.Titre, "nomGardien": oData.GardePar, 'date': oData.Date,
                        'heure': heureGarde, 'frequence': oData.Frequence, "commentaires": oData.Commentaire
                    });
                    _events[dateGarde] = element;
                    $(".responsive-calendar").responsiveCalendar('edit', _events);
                    ajouterNotification('horaire garde', oData);
                },
                error: function (err) {
                    alert(JSON.stringify(err));
                }
            });
        } else if (choix === 'rdv') {
            var dateRDV = $('#datepicker6').val();
            var heureRDV = $('#timepicker1').val();
            var adresse = $('#Adresse').val();
            var nomRDV = $('#nomrdv').val();
            var tel = $('#tel').val();
            if (commentaire === '')
                commentaire = 'Aucun commentaire';

            // $("#attente tr:first").after("<tr><td>2016-04-20</td><td>RDV</td><td>Titre: " + nomEvenement + "<br>Date du RDV: " + dateRDV + "<br>Heure du RDV: " + heureRDV + "\
            // <br>Adresse: " + adresse + " <br>Nom du spécialiste: " + nomRDV + " <br>Téléphone du spécialiste: " + tel + "<br> Commentaire du nouvel ajout: " + commentaire + " \
            //</td><td>en attente de confirmation</td></tr>");

            // appel ajax pour ajouter évènement dans RendezVous
            var sURL = "http://localhost:13340/api/RendezVous";
            var oEvenement = {
                Titre: nomEvenement,
                Commentaire: commentaire,
                Date: dateRDV,
                Adresse: adresse,
                NomPersonneRencontree: nomRDV,
                TelPersonneRencontree: tel,
            };
            $.ajax({
                url: sURL,
                async: true,
                dataType: "json",
                cache: false,
                data: oEvenement,
                type: "POST",
                success: function (oData) {
                    element.dayEvents.push({
                        "idEvent": oData.IdEvenement,
                        "type": choix,
                        "Titre": oData.Titre, "Date": oData.Date,
                        "Adresse": oData.Adresse, "NomPersonneRencontree": oData.NomPersonneRencontree,
                        "commentaires": oData.Commentaire, "TelPersonneRencontree": oData.TelPersonneRencontree
                    });
                    _events[dateRDV] = element;
                    $(".responsive-calendar").responsiveCalendar('edit', _events);
                    ajouterNotification('rendez vous', oData);

                },
                error: function (err) {
                    alert(JSON.stringify(err));
                }
            });

        } else if (choix === 'horaireVacance') {
            var deb = $('#datepicker4').val();
            var fin = $('#datepicker5').val();
            var avec = $('#GardeVacances').val();
            if (commentaire === '')
                commentaire = 'Aucun commentaire';

            // $("#attente tr:first").after("<tr><td>2016-04-20</td><td>Vacance</td><td>Titre:" + nomEvenement + "<br>\
            //Début:" + deb + "<br>Fin:" + fin + "<br>Avec:" + avec + " <br> commentaires: " + commentaire + "</td><td>en attente de confirmation</td></tr>");

            // appel ajax pour ajouter évènement dans HoraireVacances
            var sURL = "http://localhost:13340/api/HoraireVacances";
            var oEvenement = {
                Titre: nomEvenement,
                Commentaire: commentaire,
                DateDebut: deb,
                DateFin: fin,
                PersonneAccompagnon: avec
            };
            $.ajax({
                url: sURL,
                async: true,
                dataType: "json",
                cache: false,
                data: oEvenement,
                type: "POST",
                success: function (oData) {
                    element.dayEvents.push({
                        "idEvent": oData.IdEvenement,
                        "type": choix,
                        "titre": oData.Titre,
                        "commentaires": oData.Commentaire,
                        "debut": oData.DateDebut,
                        "fin": oData.DateFin,
                        "personneAccompagnon": oData.PersonneAccompagnon,

                    });
                    _events[deb] = element;
                    _events[fin] = element;
                    $(".responsive-calendar").responsiveCalendar('edit', _events);
                    ajouterNotification('horaire vacances', oData);
                },
                error: function (err) {
                    alert(JSON.stringify(err));
                }
            });
        }
    }
    );
    ///////////////////////////////////////////////////////////
    $("a.accept").click(function () {
        var element = {};
        element.number = 1;
        element.badgeClass = "badge-warning";
        element.type = choix;
        element.dayEvents = [];
        var k = $(this).parent().parent().attr('id');
        var nbre = $("#lebadge").text();
        var nombre = nbre - 1;

        $("#lebadge").text(nombre);

        $("#lebadge2").text(nombre);

        if (nombre === 0) {
            $(lebadge2).attr('style', 'background-color: #777 !important');
            $(lebadge).attr('style', 'background-color: #777 !important');
            $('#notif').modal('hide');
        }
        $(this).parent().parent().remove();

        if (k === '1') {

            element.dayEvents.push({
                "type": 'rdv', "name": "RDV qui vient d'être accepté", 'date': '2016-04-30',
                'heure': '10:00', 'adresse': '144 Boris-Rouyask, Montreal, QC, J6K 2K7', 'nomRDV': 'Dr Berger', 'tel': '514-983-9898', "commentaires": ' Doit etre à jeûn pour prise de sang'
            });
            _events['2016-04-30'] = element;

            $("#tablehistorique tr:first").after("<tr><td>2016-04-20</td><td>RDV(VIENT D'ETRE ACCEPTE)</td><td>Titre:RDV qui vient d'être accepté<br>\
		date:2016-04-30<br>heure: 10:00<br>adresse: 44 Boris-Rouyask, Montreal, QC, J6K 2K7<br>Nom du spécialiste: Dr Berger <br> Téléphone du\
		spécialiste:514-983-9898<br> Commentaire du nouvel ajout:  Doit etre à jeûn pour prise de sang</td><td>Parent1</td><td>Ajout</td></tr>");
        } else if (k === '2') {
            element.dayEvents.push({
                "type": 'horaireVacance', "name": "Vacance qui vient d'être accepté", 'debut': '2016-04-09',
                'fin': '2016-06-30', 'avec': ' parent 1(Julie)', "commentaires": 'Vacances en Europe.'
            });
            _events['2016-04-09'] = element;
            _events['2016-06-30'] = element;

            $("#tablehistorique tr:first").after("<tr><td>2016-04-20</td><td>VACANCE(VIENT D'ETRE ACCEPTE)</td><td>Titre:Vacance qui vient d'être accepté<br>\
		Début:2016-05-30<br>Fin:2016-06-30 <br>Avec: parent 1(Julie) <br> commentaires: Vacances en Europe</td><td>Parent1</td><td>Ajout</td></tr>");
        }
        $(".responsive-calendar").responsiveCalendar('edit', _events);
    });
    //////////////////////////////////////////
    $("a.cancel").click(function () {
        var k = $(this).parent().parent().attr('id');
        var nbre = $("#lebadge").text();
        var nombre = nbre - 1;

        $("#lebadge").text(nombre);

        $("#lebadge2").text(nombre);
        if (nombre === 0) {
            $(lebadge2).attr('style', 'background-color: #777 !important');
            $(lebadge).attr('style', 'background-color: #777 !important');
            $('#notif').modal('hide');
        }
        if (k === '1') {
            $("#tablehistorique tr:first").after("<tr><td>2016-04-20</td><td>RDV(VIENT D'ETRE REFUSE)</td><td>Titre:RDV qui vient d'être refusé<br>\
		date:2016-04-30<br>heure: 10:00<br>adresse: 44 Boris-Rouyask, Montreal, QC, J6K 2K7<br>Nom du spécialiste: Dr Berger <br> Téléphone du\
		spécialiste:514-983-9898<br> Commentaire du nouvel ajout:  Doit etre à jeûn pour prise de sang</td><td>Parent1</td><td>Rejet </td></tr>");
        } else if (k === '2') {

            $("#tablehistorique tr:first").after("<tr><td>2016-04-20</td><td>Vacance(VIENT D'ETRE REFUSE)</td><td>Titre:Vacance qui vient d'être refusé<br>\
		Début:2016-05-30<br>Fin:2016-06-30 <br>Avec: parent 1(Julie) <br> commentaires: Vacances en Europe</td><td>Parent1</td><td>Ajout</td></tr>");
        }
        $(this).parent().parent().remove();

    });
    ////////////////////////////////////////////

    $("#editScolaire").click(function () {
        $("#titreScolaire").removeAttr('disabled');
        $("#FreqScolaire").removeAttr('disabled');
        $("#deScolaire").removeAttr('disabled');
        $("#aScolaire").removeAttr('disabled');
        $("#noteScolaire").removeAttr('disabled');
        $("#tablehistorique tr:first").after("<tr><td>2016-04-20</td><td>Horaire scolaire(VIENT D'ETRE MODIFIE)</td><td>Titre: TitreModifié<br>\
		Fréquence: frequenceModifiée<br>Debut année scolaire: debutModifié<br>Fin session scolaire: finModifiée<br>Commentaires: \
		commentaireModifié<br></td><td>Parent1</td><td>Suppression</td></tr>");

    });
    /////////////////////////////////////
    $("#editrdv").click(function () {
        $("#titrerdv").removeAttr('disabled');
        $("#daterdv").removeAttr('disabled');
        $("#ardv").removeAttr('disabled');
        $("#endroit").removeAttr('disabled');
        $("#nomRdv").removeAttr('disabled');
        $("#telrdv").removeAttr('disabled');
        $("#noterdv").removeAttr('disabled');
        $("#tablehistorique tr:first").after("<tr><td>2016-04-20</td><td>RDV(VIENT D'ETRE MODIFIÉ)</td><td>Titre:RDV qui vient d'être modifié<br>\
		date:DateRdvModifiée<br>heure: heureRDVModifiée<br>adresse: adresse qui vient d'être modifiée<br>Nom du spécialiste: SpécialisteModifié <br> Téléphone du \ spécialiste:telephoneModifié<br> commentaires: commentairesModifiés</td><td>Parent1</td><td>Modification</td></tr>");

    });

    ////////////////////////////////////////////

    $("#editVac").click(function () {
        $("#titreVac").removeAttr('disabled');
        $("#deVac").removeAttr('disabled');
        $("#aVac").removeAttr('disabled');
        $("#avecVac").removeAttr('disabled');
        $("#noteVac").removeAttr('disabled');
        $("#tablehistorique tr:first").after("<tr><td>2016-04-20</td><td>Vacance(VIENT D'ETRE MODIFIEE)</td><td>Titre:Vacance qui vient d'être modifiée<br>\
		Début:dateDebutModifiée<br>Fin:dateDeFinModifiée <br>Avec: parentModifié <br> commentaires: commentairesModifié</td><td>Parent1</td><td>Modification</td></tr>");
    });

    ////////////////////////////////////////////

    $("#editGarde").click(function () {
        $("#titreGarde").removeAttr('disabled');
        $("#nomGard").removeAttr('disabled');
        $("#dateGarde").removeAttr('disabled');
        $("#heureGarde").removeAttr('disabled');
        $("#noteGarde").removeAttr('disabled');
        $("#frequencee").removeAttr('disabled');
        $("#tablehistorique tr:first").after("<tr><td>2016-04-20</td><td>Horaire de garde(VIENT D'ETRE MODIFIE)</td><td>Titre: TitreModifié<br>\
		Gardé par: NomModifie<br>Le: jourModifié<br>A: heureModifiée<br>Fréquence de garde: fréquenceModifiée<br>Commentaires: commentaireModifié<br></td><td>Parent1</td><td>Suppression</td></tr>");
    });

    ////////////////////////////////////////////
    $("#okscolaire").click(function () {

        var idEvenementP = $("#idEvenement").val();
        // appel ajax pour ajouter évènement dans Horaire scolaire
        var sURL = "http://localhost:13340/api/HoraireScolaire/" + idEvenementP;
        var oEvenement = {
            IdEvenement: idEvenementP,
            Titre: $("#titreScolaire").val(),
            JoursDeCours: $("#FreqScolaire").val(),
            DebutSession: $("#deScolaire").val(),
            FinSession: $("#aScolaire").val(),
            Commentaire: $("#noteScolaire").val()
        };

        $.ajax({
            url: sURL,
            async: true,
            dataType: "json",
            cache: false,
            data: oEvenement,
            type: "PUT",
            success: function (oData) {
                var element = {};
                element.number = 1;
                element.badgeClass = "badge-warning";
                element.type = choix;
                element.dayEvents = [];

                var debut = $('#deScolaire').val();
                var fin = $('#aScolaire').val();

                var tab1 = [];

                tab1[0] = debut;

                tab1[1] = fin;

                $('.responsive-calendar').responsiveCalendar('clear', tab1);
                delete _events[tab1[0]];
                delete _events[tab1[1]];

                alert("Modification fait avec succés!")
                //$(".responsive-calendar").responsiveCalendar('edit', oData);     
                element.dayEvents.push({
                    "idEvent": oData.IdEvenement,
                    "type": choix, "date": oData.DebutSession,
                    "fin": oData.FinSession,
                    "name": oData.Titre,
                    "commentaires": oData.Commentaire
                });

                _events[debut] = element;
                _events[fin] = element;
                $(".responsive-calendar").responsiveCalendar('edit', _events);

                $('#vueEvenScolaire').modal('hide');
            },
            error: function (err) {
                alert(JSON.stringify(err));
            }
        });

    });
    /////////////////////////////////////////////////
    $('#vueEvenScolaire').on('hidden.bs.modal', function (e) {
        $("#titreScolaire").attr("disabled", 'disabled');
        $("#FreqScolaire").attr("disabled", 'disabled');
        $("#deScolaire").attr("disabled", 'disabled');
        $("#aScolaire").attr("disabled", 'disabled');
        $("#noteScolaire").attr("disabled", 'disabled');
    })
    ////////////////////////////////////////////
    $("#okGarde").click(function () {
        //        $('#vueEvenGarde').modal('hide');
        var idEvenementP = $("#idEvenement").val();
        // appel ajax pour ajouter évènement dans Horaire scolaire
        var sURL = "http://localhost:13340/api/HoraireGarde/" + idEvenementP;
        
       
        var oEvenement = {
            IdEvenement: idEvenementP,
            Titre: $("#titreGarde").val(),
            Commentaire: $("#commentaires").val(),
            GardePar: $("#nomGard").val(),
            Frequence: $("#frequencee").val(),
            Date: $("#dateGarde").val()
        };

        $.ajax({
            url: sURL,
            async: true,
            dataType: "json",
            cache: false,
            data: oEvenement,
            type: "PUT",
            success: function (oData) {
                var element = {};
                element.number = 1;
                element.badgeClass = "badge-warning";
                element.type = choix;
                element.dayEvents = [];

                var debut = oData.Date;
                var fin = oData.Date;

                var tab1 = [];

                tab1[0] = debut;

                tab1[1] = fin;

                $('.responsive-calendar').responsiveCalendar('clear', tab1);
                delete _events[tab1[0]];
                delete _events[tab1[1]];

                alert("Modification fait avec succés!")

                element.dayEvents.push({
                    "idEvent": oData.IdEvenement,
                    "type": choix, "name": oData.Titre, "nomGardien": oData.GardePar, 'date': oData.Date,
                    'heure': heureGarde, 'frequence': oData.Frequence, "commentaires": oData.Commentaire
                });
                _events[dateGarde] = element;
                $(".responsive-calendar").responsiveCalendar('edit', _events);
                $('#vueEvenGarde').modal('hide');
            },
            error: function (err) {
                alert(JSON.stringify(err));
            }
        });



    });
    //////////////////////////////////////////////
    $('#vueEvenGarde').on('hidden.bs.modal', function (e) {

        $("#titreGarde").attr("disabled", 'disabled');
        $("#nomGard").attr("disabled", 'disabled');
        $("#dateGarde").attr("disabled", 'disabled');
        $("#heureGarde").attr("disabled", 'disabled');
        $("#noteGarde").attr("disabled", 'disabled');
        $("#frequencee").attr("disabled", 'disabled');

    })
    /////////////////////////////////////////////

    $("#okvac").click(function () {

        // $('#vuevacances').modal('hide');
    });
    ////////////////////////////////
    $('#vuevacances').on('hidden.bs.modal', function (e) {
        $("#titreVac").attr("disabled", 'disabled');
        $("#deVac").attr("disabled", 'disabled');
        $("#aVac").attr("disabled", 'disabled');
        $("#avecVac").attr("disabled", 'disabled');
        $("#noteVac").attr("disabled", 'disabled');
    })
    ///////////////////////////////////////

    $("#okrdv").click(function () {

        $('#vuerdv').modal('hide');

    });
    ////////////////////////////////////
    $('#vuerdv').on('hidden.bs.modal', function (e) {
        $("#titrerdv").attr("disabled", 'disabled');
        $("#daterdv").attr("disabled", 'disabled');
        $("#ardv").attr("disabled", 'disabled');
        $("#endroit").attr("disabled", 'disabled');
        $("#nomRdv").attr("disabled", 'disabled');
        $("#telrdv").attr("disabled", 'disabled');
        $("#noterdv").attr("disabled", 'disabled');
    })
    /////////////////
    $("#deleterdv").click(function () {
        var date = $('#daterdv').val();
        var tab = [];
        tab[0] = date;
        $('.responsive-calendar').responsiveCalendar('clear', tab);
        delete _events[date];
        // $('#vuerdv').modal('hide');
        // $("#tablehistorique tr:first").after("<tr><td>2016-04-20</td><td>RDV(VIENT D'ETRE SUPPRIME)</td><td>Titre: " + $('#titrerdv').val() + "<br>Date du RDV: " + $('#daterdv').val() + "<br>Heure du RDV: " + $('#ardv').val() + "<br>Adresse: " + $('#endroit').val() + " <br>Nom du spécialiste: " + $('#nomRdv').val() + " <br>Téléphone du spécialiste: " + $('#telrdv').val() + "<br> Commentaire du nouvel ajout: " + $('#noterdv').val() + " \
        //<br></td><td>Parent1</td><td>Suppression</td></tr>");

        // appel ajax pour supprimer évènement dans RendezVous
        var date = $('#daterdv').val().substring(0, 10);
        var oEvent = _events[date].dayEvents[0];
        var id = oEvent.idEvent;

        var sURL = "http://localhost:13340/api/RendezVous/" + id;

        $.ajax({
            url: sURL,
            async: true,
            cache: false,
            type: "DELETE",
            success: function (rep) {
                $("#vuerdv").modal('hide');
            },
            error: function (err) {
                alert(JSON.stringify(err));
            }
        });

    });
    /////////////////
    $("#deleteScolaire").click(function () {
        var tab1 = [];
        var date = $('#deScolaire').val();
        tab1[0] = date;
        date = $('#aScolaire').val();
        tab1[1] = date;

        $('.responsive-calendar').responsiveCalendar('clear', tab1);
        delete _events[tab1[0]];
        delete _events[tab1[1]];

        // $('#vueEvenScolaire').modal('hide');
        // $("#tablehistorique tr:first").after("<tr><td>2016-04-20</td><td>Horaire scolaire(VIENT D'ETRE SUPPRIMEE)</td><td>Titre: " + $('#titreScolaire').val() + "<br>\
        //	Frequence: 	" + $('#FreqScolaire').val() + "<br>Debut de l'année scolaire: " + $('#deScolaire').val() + "<br>Fin de l'année scolaire: " + $('#aScolaire').val() + "<br>Commentaires: " + $('#noteScolaire').val() + "<br></td><td>Parent1</td><td>Suppression</td></tr>");

        // appel ajax pour supprimer évènement dans HoraireScolaire
        var date = $('#deScolaire').val().substring(0, 10);
        var oEvent = _events[date].dayEvents[0];
        var id = oEvent.idEvent;

        var sURL = "http://localhost:13340/api/HoraireScolaire/" + id;

        $.ajax({
            url: sURL,
            async: true,
            cache: false,
            type: "DELETE",
            success: function (rep) {
                $("#vueEvenScolaire").modal('hide');
            },
            error: function (err) {
                alert(JSON.stringify(err));
            }
        });
    });
    /////////////////  
    $("#deleteVac").click(function () {
        var tab2 = [];
        var date = $('#deVac').val();
        tab2[0] = date;
        date = $('#aVac').val();
        tab2[1] = date;

        $('.responsive-calendar').responsiveCalendar('clear', tab2);
        delete _events[tab2[0]];
        delete _events[tab2[0]];
        // $('#vuevacances').modal('hide');
        // $("#tablehistorique tr:first").after("<tr><td>2016-04-20</td><td>Vacance(VIENT D'ETRE SUPPRIMEE)</td><td>Titre:" + $('#titreVac').val() + "<br>\
        //	Début:" + $('#deVac').val() + "<br>Fin:" + $('#aVac').val() + "<br>Avec:" + $('#avecVac').val() + " <br> commentaires: " + $('#noteVac').val()
        //       + "</td><td>Parent1</td><td>Suppression</td></tr>");

        // appel ajax pour supprimer évènement dans HoraireVacances
        var date = $('#deVac').val().substring(0, 10);
        var oEvent = _events[date].dayEvents[0];
        var id = oEvent.idEvent;

        var sURL = "http://localhost:13340/api/HoraireVacances/" + id;

        $.ajax({
            url: sURL,
            async: true,
            cache: false,
            type: "DELETE",
            success: function (rep) {
                $("#vuevacances").modal('hide');
            },
            error: function (err) {
                alert(JSON.stringify(err));
            }
        });
    });
    ///////////////////
    $("#deleteGarde").click(function () {
        var tab3 = [];
        var date = $('#dateGarde').val();
        tab3[0] = date;
        $('.responsive-calendar').responsiveCalendar('clear', tab3);
        delete _events[tab3[0]];

        // $('#vueEvenGarde').modal('hide');
        // $("#tablehistorique tr:first").after("<tr><td>2016-04-20</td><td>Horaire de garde(VIENT D'ETRE SUPPRIME)</td><td>Titre: " + $('#titreGarde').val() + "<br>\
        //	Gardé par: 	" + $('#nomGard').val() + "<br>Le: " + $('#dateGarde').val() + "<br>A: " + $('#heureGarde').val() + "<br>Fréquence de garde: " + $('#frequencee').val() + "<br>Commentaires: " + $('#commentaires').val() + "<br></td><td>Parent1</td><td>Suppression</td></tr>");

        // appel ajax pour supprimer évènement dans HoraireGarde
        var date = $('#dateGarde').val().substring(0, 10);
        var oEvent = _events[date].dayEvents[0];
        var id = oEvent.idEvent;

        var sURL = "http://localhost:13340/api/HoraireGarde/" + id;

        $.ajax({
            url: sURL,
            async: true,
            cache: false,
            type: "DELETE",
            success: function (rep) {
                $("#vueEvenGarde").modal('hide');
            },
            error: function (err) {
                alert(JSON.stringify(err));
            }
        });
    });


    function ajouterNotification(type, oData) {
        var notificationcnt = $('#notificationcnt').text();
        var cnt = Number(notificationcnt) + 1;
        $('#notificationcnt').text(cnt.toString());
        $('#notificationcntIndex').text(cnt.toString());
        var notification = " <tr id='" + cnt + "'>" +
        "<td>" + (new Date()).toLocaleDateString() + "</td>" +
        "<td>" +
        " Type de demande: " + type + " <br>" +
        " Titre: " + oData.Titre + "<br>" +
        " Date debut: " + (new Date(oData.DebutSession)).toLocaleDateString() + "<br>" +
        " Commentaires:" + oData.Commentaire + "<br>" +
         "</td>" +
            "<td><a class='accept'><i class='fa fa-check'> </a></td>" +
        "<td><a class='cancel'><span class='glyphicon glyphicon-remove'> </span></a></td>" +
     "</tr>";

        $('#not tbody').append(notification);
    }
});