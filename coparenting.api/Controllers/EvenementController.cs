﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using coparenting.api.Models;

namespace coparenting.api.Controllers
{
    public class EvenementController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Evenement
        public IQueryable<EvenementModels> GetEvenementModels()
        {
            return db.EvenementModels;
        }

        // GET: api/Evenement/5
        [ResponseType(typeof(EvenementModels))]
        public IHttpActionResult GetEvenementModels(int id)
        {
            EvenementModels evenementModels = db.EvenementModels.Find(id);
            if (evenementModels == null)
            {
                return NotFound();
            }

            return Ok(evenementModels);
        }

        // PUT: api/Evenement/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutEvenementModels(int id, EvenementModels evenementModels)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != evenementModels.IdEvenement)
            {
                return BadRequest();
            }

            db.Entry(evenementModels).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EvenementModelsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Evenement
        [ResponseType(typeof(EvenementModels))]
        public IHttpActionResult PostEvenementModels(EvenementModels evenementModels)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.EvenementModels.Add(evenementModels);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = evenementModels.IdEvenement }, evenementModels);
        }

        // DELETE: api/Evenement/5
        [ResponseType(typeof(EvenementModels))]
        public IHttpActionResult DeleteEvenementModels(int id)
        {
            EvenementModels evenementModels = db.EvenementModels.Find(id);
            if (evenementModels == null)
            {
                return NotFound();
            }

            db.EvenementModels.Remove(evenementModels);
            db.SaveChanges();

            return Ok(evenementModels);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EvenementModelsExists(int id)
        {
            return db.EvenementModels.Count(e => e.IdEvenement == id) > 0;
        }
    }
}