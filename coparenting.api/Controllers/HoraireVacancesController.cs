﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using coparenting.api.Models;

namespace coparenting.api.Controllers
{
    public class HoraireVacancesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/HoraireVacancesModels
        public IQueryable<HoraireVacancesModels> GetHoraireVacancesModels()
        {
            return db.HoraireVacancesModels;
        }

        // GET: api/HoraireVacancesModels/5
        [ResponseType(typeof(HoraireVacancesModels))]
        public IHttpActionResult GetHoraireVacancesModels(int id)
        {
            HoraireVacancesModels horaireVacancesModels = db.HoraireVacancesModels.Find(id);
            if (horaireVacancesModels == null)
            {
                return NotFound();
            }

            return Ok(horaireVacancesModels);
        }

        // PUT: api/HoraireVacancesModels/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutHoraireVacancesModels(int id, HoraireVacancesModels horaireVacancesModels)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != horaireVacancesModels.IdEvenement)
            {
                return BadRequest();
            }

            db.Entry(horaireVacancesModels).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HoraireVacancesModelsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/HoraireVacancesModels
        [ResponseType(typeof(HoraireVacancesModels))]
        public IHttpActionResult PostHoraireVacancesModels(HoraireVacancesModels horaireVacancesModels)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.HoraireVacancesModels.Add(horaireVacancesModels);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = horaireVacancesModels.IdEvenement }, horaireVacancesModels);
        }

        // DELETE: api/HoraireVacancesModels/5
        [ResponseType(typeof(HoraireVacancesModels))]
        public IHttpActionResult DeleteHoraireVacancesModels(int id)
        {
            HoraireVacancesModels horaireVacancesModels = db.HoraireVacancesModels.Find(id);
            if (horaireVacancesModels == null)
            {
                return NotFound();
            }

            db.HoraireVacancesModels.Remove(horaireVacancesModels);
            db.SaveChanges();

            return Ok(horaireVacancesModels);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool HoraireVacancesModelsExists(int id)
        {
            return db.HoraireVacancesModels.Count(e => e.IdEvenement == id) > 0;
        }
    }
}