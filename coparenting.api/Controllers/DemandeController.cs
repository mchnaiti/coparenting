﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using coparenting.api.Models;

namespace coparenting.api.Controllers
{
    public class DemandeController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Demande
        public IQueryable<DemandeModels> GetDemandeModels()
        {
            return db.DemandeModels;
        }

        // GET: api/Demande/5
        [ResponseType(typeof(DemandeModels))]
        public IHttpActionResult GetDemandeModels(int id)
        {
            DemandeModels demandeModels = db.DemandeModels.Find(id);
            if (demandeModels == null)
            {
                return NotFound();
            }

            return Ok(demandeModels);
        }

        // PUT: api/Demande/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDemandeModels(int id, DemandeModels demandeModels)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != demandeModels.Id)
            {
                return BadRequest();
            }

            db.Entry(demandeModels).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DemandeModelsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Demande
        [ResponseType(typeof(DemandeModels))]
        public IHttpActionResult PostDemandeModels(DemandeModels demandeModels)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.DemandeModels.Add(demandeModels);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = demandeModels.Id }, demandeModels);
        }

        // DELETE: api/Demande/5
        [ResponseType(typeof(DemandeModels))]
        public IHttpActionResult DeleteDemandeModels(int id)
        {
            DemandeModels demandeModels = db.DemandeModels.Find(id);
            if (demandeModels == null)
            {
                return NotFound();
            }

            db.DemandeModels.Remove(demandeModels);
            db.SaveChanges();

            return Ok(demandeModels);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DemandeModelsExists(int id)
        {
            return db.DemandeModels.Count(e => e.Id == id) > 0;
        }
    }
}