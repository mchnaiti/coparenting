﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using coparenting.api.Models;

namespace coparenting.api.Controllers
{
    public class HoraireGardeController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/HoraireGardeModels
        public IQueryable<HoraireGardeModels> GetHoraireGardeModels()
        {
            return db.HoraireGardeModels;
        }

        // GET: api/HoraireGardeModels/5
        [ResponseType(typeof(HoraireGardeModels))]
        public IHttpActionResult GetHoraireGardeModels(int id)
        {
            HoraireGardeModels horaireGardeModels = db.HoraireGardeModels.Find(id);
            if (horaireGardeModels == null)
            {
                return NotFound();
            }

            return Ok(horaireGardeModels);
        }

        // PUT: api/HoraireGardeModels/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutHoraireGardeModels(int id, HoraireGardeModels horaireGardeModels)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != horaireGardeModels.IdEvenement)
            {
                return BadRequest();
            }

            db.Entry(horaireGardeModels).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HoraireGardeModelsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(horaireGardeModels);
        }

        // POST: api/HoraireGardeModels
        [ResponseType(typeof(HoraireGardeModels))]
        public IHttpActionResult PostHoraireGardeModels(HoraireGardeModels horaireGardeModels)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.HoraireGardeModels.Add(horaireGardeModels);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = horaireGardeModels.IdEvenement }, horaireGardeModels);
        }

        // DELETE: api/HoraireGardeModels/5
        [ResponseType(typeof(HoraireGardeModels))]
        public IHttpActionResult DeleteHoraireGardeModels(int id)
        {
            HoraireGardeModels horaireGardeModels = db.HoraireGardeModels.Find(id);
            if (horaireGardeModels == null)
            {
                return NotFound();
            }

            db.HoraireGardeModels.Remove(horaireGardeModels);
            db.SaveChanges();

            return Ok(horaireGardeModels);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool HoraireGardeModelsExists(int id)
        {
            return db.HoraireGardeModels.Count(e => e.IdEvenement == id) > 0;
        }
    }
}