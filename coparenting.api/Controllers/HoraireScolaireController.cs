﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using coparenting.api.Models;

namespace coparenting.api.Controllers
{
    public class HoraireScolaireController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/HoraireScolaire
        public IQueryable<HoraireScolaireModels> GetHoraireScolaireModels()
        {
            return db.HoraireScolaireModels;
        }

        // GET: api/HoraireScolaire/5
        [ResponseType(typeof(HoraireScolaireModels))]
        public IHttpActionResult GetHoraireScolaireModels(int id)
        {
            HoraireScolaireModels horaireScolaireModels = db.HoraireScolaireModels.Find(id);
            if (horaireScolaireModels == null)
            {
                return NotFound();
            }

            return Ok(horaireScolaireModels);
        }

        // PUT: api/HoraireScolaire/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutHoraireScolaireModels(int id, HoraireScolaireModels horaireScolaireModels)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != horaireScolaireModels.IdEvenement)
            {
                return BadRequest();
            }

            db.Entry(horaireScolaireModels).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HoraireScolaireModelsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(horaireScolaireModels);
        }

        // POST: api/HoraireScolaire
        [ResponseType(typeof(HoraireScolaireModels))]
        public IHttpActionResult PostHoraireScolaireModels(HoraireScolaireModels horaireScolaireModels)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.HoraireScolaireModels.Add(horaireScolaireModels);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = horaireScolaireModels.IdEvenement }, horaireScolaireModels);
        }

        // DELETE: api/HoraireScolaire/5
        [ResponseType(typeof(HoraireScolaireModels))]
        public IHttpActionResult DeleteHoraireScolaireModels(int id)
        {
            HoraireScolaireModels horaireScolaireModels = db.HoraireScolaireModels.Find(id);
            if (horaireScolaireModels == null)
            {
                return NotFound();
            }

            db.HoraireScolaireModels.Remove(horaireScolaireModels);
            db.SaveChanges();

            return Ok(horaireScolaireModels);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool HoraireScolaireModelsExists(int id)
        {
            return db.HoraireScolaireModels.Count(e => e.IdEvenement == id) > 0;
        }
    }
}