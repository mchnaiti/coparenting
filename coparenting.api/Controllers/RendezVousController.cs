﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using coparenting.api.Models;

namespace coparenting.api.Controllers
{
    public class RendezVousController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/RendezVousModels
        public IQueryable<RendezVousModels> GetRendezVousModels()
        {
            return db.RendezVousModels;
        }

        // GET: api/RendezVousModels/5
        [ResponseType(typeof(RendezVousModels))]
        public IHttpActionResult GetRendezVousModels(int id)
        {
            RendezVousModels rendezVousModels = db.RendezVousModels.Find(id);
            if (rendezVousModels == null)
            {
                return NotFound();
            }

            return Ok(rendezVousModels);
        }

        // PUT: api/RendezVousModels/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutRendezVousModels(int id, RendezVousModels rendezVousModels)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != rendezVousModels.IdEvenement)
            {
                return BadRequest();
            }

            db.Entry(rendezVousModels).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RendezVousModelsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/RendezVousModels
        [ResponseType(typeof(RendezVousModels))]
        public IHttpActionResult PostRendezVousModels(RendezVousModels rendezVousModels)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.RendezVousModels.Add(rendezVousModels);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = rendezVousModels.IdEvenement }, rendezVousModels);
        }

        // DELETE: api/RendezVousModels/5
        [ResponseType(typeof(RendezVousModels))]
        public IHttpActionResult DeleteRendezVousModels(int id)
        {
            RendezVousModels rendezVousModels = db.RendezVousModels.Find(id);
            if (rendezVousModels == null)
            {
                return NotFound();
            }

            db.RendezVousModels.Remove(rendezVousModels);
            db.SaveChanges();

            return Ok(rendezVousModels);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RendezVousModelsExists(int id)
        {
            return db.RendezVousModels.Count(e => e.IdEvenement == id) > 0;
        }
    }
}