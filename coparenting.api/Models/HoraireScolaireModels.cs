﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace coparenting.api.Models
{
    public class HoraireScolaireModels:EvenementModels
    {
        public String Period { get; set; }
        public DateTime DebutSession { get; set; }
        public DateTime FinSession { get; set; }
    }
}