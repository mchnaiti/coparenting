﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace coparenting.api.Models
{
    public class DemandeModels
    {
        public int Id { get; set; }
        public DateTime DateDemande { get; set; }
        public EvenementModels Evenement { get; set; }
        public Boolean Acceptee { get; set; }
        public string EffectueePar { get; set; }
    }
}