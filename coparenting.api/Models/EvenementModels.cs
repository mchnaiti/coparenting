﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace coparenting.api.Models
{
    public class EvenementModels
    {
        [Key]
        public int IdEvenement { get; set; }

        public string Titre { get; set; }
        public string Commentaire { get; set; }
    }
}