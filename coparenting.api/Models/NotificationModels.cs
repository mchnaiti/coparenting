﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace coparenting.api.Models
{
    public class NotificationModels
    {
        [Key]
        public DateTime Date { get; set; }
        public String Type { get; set; }
        public String ContenuNotification { get; set; }
        public String NomAuteur { get; set; }
        public String NomDestinateur { get; set; }
        public String Statut { get; set; }
    }
}