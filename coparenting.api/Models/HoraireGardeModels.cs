﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace coparenting.api.Models
{
    public class HoraireGardeModels:EvenementModels
    {
        public String GardePar { get; set; }
        public DateTime Date { get; set; }
        public String Frequence { get; set; }
    }
}