﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;

namespace coparenting.api.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }
        
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<coparenting.api.Models.EvenementModels> EvenementModels { get; set; }

        public System.Data.Entity.DbSet<coparenting.api.Models.HoraireScolaireModels> HoraireScolaireModels { get; set; }

        public System.Data.Entity.DbSet<coparenting.api.Models.RendezVousModels> RendezVousModels { get; set; }

        public System.Data.Entity.DbSet<coparenting.api.Models.HoraireGardeModels> HoraireGardeModels { get; set; }

        public System.Data.Entity.DbSet<coparenting.api.Models.HoraireVacancesModels> HoraireVacancesModels { get; set; }

        public System.Data.Entity.DbSet<coparenting.api.Models.DemandeModels> DemandeModels { get; set; }
        public System.Data.Entity.DbSet<coparenting.api.Models.UsersModel> UsersModels { get; set; }
    }
}