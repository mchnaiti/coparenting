﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace coparenting.api.Models
{
    public class HoraireVacancesModels:EvenementModels
    {
        public DateTime DateDebut { get; set; }
        public DateTime DateFin { get; set; }
        public string PersonneAccompagnon { get; set; }
    }
}