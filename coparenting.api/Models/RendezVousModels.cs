﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace coparenting.api.Models
{
    public class RendezVousModels:EvenementModels
    {
        public DateTime Date { get; set; }
        public String Adresse { get; set; }
        public String NomPersonneRencontree { get; set; }
        public String TelPersonneRencontree { get; set; }
    }
}